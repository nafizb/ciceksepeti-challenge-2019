package com.a3rdwavecoders.ciceksepetichallenge.models;

import android.location.Location;

import com.a3rdwavecoders.ciceksepetichallenge.R;

public class Order {
    String name;
    Location location;
    String assignedStoreName = "-";

    public Order(String name, String lat, String lng) {
        this.name = name;
        this.location = new Location(name);
        location.setLatitude(Double.valueOf(lat));
        location.setLongitude(Double.valueOf(lng));
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public void assign(Store store) {
        assignedStoreName = store.getName();
    }
    public int getIconResource() {
        if (assignedStoreName.equals("Kirmizi")) {
            return R.drawable.ic_red_order;
        } else if(assignedStoreName.equals("Yesil")) {
            return R.drawable.ic_green_order;
        } else if(assignedStoreName.equals("Mavi")) {
            return R.drawable.ic_blue_order;
        } else {
            return R.drawable.ic_neutral_order;
        }
     }
}
