package com.a3rdwavecoders.ciceksepetichallenge;

import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.a3rdwavecoders.ciceksepetichallenge.models.Order;
import com.a3rdwavecoders.ciceksepetichallenge.models.Store;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.opencsv.CSVReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;

public class MainActivity extends AppCompatActivity {
    private MapView mapView;
    FloatingActionButton fab;

    ArrayList<Store> stores = new ArrayList<>();
    ArrayList<Order> orders = new ArrayList<>();

    Style mapStyle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1IjoibmFmaXpiIiwiYSI6ImNqc3JmMHh0cDFobm80OXBncm8wcXB6YTcifQ.4lHmEceG4VIz9rGQWOUq-g");
        setContentView(R.layout.activity_main);

        loadStoreData();
        loadOrderData();

        fab = findViewById(R.id.fab);
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull final MapboxMap mapboxMap) {
                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        mapStyle = style;
                        for (Order order: orders) {
                            addOrderMarker(style, order);
                        }
                        for (Store store: stores) {
                            addStoreMarker(style, store);
                        }
                    }
                });
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Assigning Orders", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                removeAllOrderMarkers(mapStyle);
                assignOrders();
                for (Order order: orders) {
                    addOrderMarker(mapStyle, order);
                }
                for (Store store: stores) {
                    addStoreMarker(mapStyle, store);
                }
            }
        });
    }
    void loadStoreData() {
        stores.add(new Store("Kirmizi","41.049792", "29.003031", R.drawable.ic_red_store, 20, 30 ));
        stores.add(new Store("Yesil","41.069940", "29.019250",R.drawable.ic_green_store, 35,50 ));
        stores.add(new Store("Mavi","41.049997", "29.026108", R.drawable.ic_blue_store, 20,80));
    }

    void addStoreMarker(Style style, Store store) {
        style.addImage(store.getName(),
                BitmapFactory.decodeResource(
                        MainActivity.this.getResources(), store.getIconResource()));

        GeoJsonSource geoJsonSource = new GeoJsonSource(store.getName(), Feature.fromGeometry(
                Point.fromLngLat(store.getLocation().getLongitude(), store.getLocation().getLatitude())));
        style.addSource(geoJsonSource);

        SymbolLayer symbolLayer = new SymbolLayer(store.getName(), store.getName());
        symbolLayer.withProperties(
                PropertyFactory.iconImage(store.getName())
        );
        style.addLayer(symbolLayer);
    }


    Store getClosestStore(List<Store> storeList, Order order) {
        float minDistance = Float.MAX_VALUE;
        Store closestStore = null;
        for (Store store: storeList) {
            float dist = store.getDistance(order.getLocation());

            if(dist < minDistance) {
                minDistance = dist;
                closestStore = store;
            }
        }

        return closestStore;
    }

    void assignOrders() {
        for (Store store: stores) {
            store.setAssigned(0);
        }

        for (Order order: orders) {

            ArrayList<Store> storeList = (ArrayList<Store>) stores.clone();
            Store closestStore = getClosestStore(storeList, order);


            while (!closestStore.isAvailable()) {
                storeList.remove(closestStore);
                closestStore = getClosestStore(storeList, order);
            }

            while (closestStore.isSatisfied() && storeList.size() >= 2 && isThereAnyUnsatisfiedStore(storeList)) {
                storeList.remove(closestStore);
                closestStore = getClosestStore(storeList, order);
            }

            order.assign(closestStore);
            closestStore.assign();

        }
        new AlertDialog.Builder(this)
                .setTitle("Sipariş atama raporu")
                .setMessage("Kırmızı Dükkan: " + stores.get(0).getAssigned() + "\n"
                + "Yeşil dükkan: " + stores.get(1).getAssigned() + "\n"
                + "Mavi dükkan: " + stores.get(2).getAssigned() + "\n")


                .setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })

                .show();
        Log.e("kir", stores.get(0).getAssigned() + ".");
        Log.e("yeşil", stores.get(1).getAssigned() + ".");
        Log.e("mavi", stores.get(2).getAssigned() + ".");
    }

    boolean isThereAnyUnsatisfiedStore(List<Store> storeList) {
        for (Store store: storeList) {
            if (!store.isSatisfied()) {
                return true;
            }
        }
        return false;
    }
    void addOrderMarker(Style style, Order order) {
        style.addImage(order.getName(),
                BitmapFactory.decodeResource(
                        MainActivity.this.getResources(), order.getIconResource()));

        GeoJsonSource geoJsonSource = new GeoJsonSource(order.getName(), Feature.fromGeometry(
                Point.fromLngLat(order.getLocation().getLongitude(), order.getLocation().getLatitude())));
        style.addSource(geoJsonSource);

        SymbolLayer symbolLayer = new SymbolLayer(order.getName(), order.getName());
        symbolLayer.withProperties(
                PropertyFactory.iconImage(order.getName()),
                iconAllowOverlap(true)
        );
        style.addLayer(symbolLayer);
    }

    void removeAllOrderMarkers(Style style) {
        for (Order order: orders) {
            style.removeLayer(order.getName());
            style.removeSource(order.getName());
            style.removeImage(order.getName());
        }
        for (Store store: stores) {
            style.removeLayer(store.getName());
            style.removeSource(store.getName());
            style.removeImage(store.getName());
        }
    }

    void loadOrderData() {
        InputStreamReader is = null;
        try {
            is = new InputStreamReader(getAssets()
                    .open("siparis_cords.csv"));
            BufferedReader reader = new BufferedReader(is);
            reader.readLine();
            String line;
            while ((line = reader.readLine()) != null) {
                String[] columns = line.split(";");
                orders.add(new Order(columns[0], columns[1], columns[2]));
            }

        } catch (IOException e) {
            Log.e("err", e.getMessage());
            e.printStackTrace();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }
}
