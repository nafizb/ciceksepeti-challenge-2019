package com.a3rdwavecoders.ciceksepetichallenge.models;

import android.location.Location;

public class Store {
    String name;
    Location location;
    int iconResource;

    int assigned = 0;
    int minQuota = 0;
    int maxQuota = 0;
    public Store(String name, String lat, String lng, int iconResource, int minQuota, int maxQuota) {
        this.name = name;
        this.location = new Location(name);
        location.setLatitude(Double.valueOf(lat));
        location.setLongitude(Double.valueOf(lng));

        this.iconResource = iconResource;
        this.minQuota = minQuota;
        this.maxQuota = maxQuota;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public float getDistance(Location target) {
        return location.distanceTo(target);
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }

    public boolean isAvailable() {
        return assigned < maxQuota;
    }

    public boolean isSatisfied() {
        return assigned >= minQuota;
    }

    public void assign() {
        assigned++;
    }
    public int getAssigned() {
        return assigned;
    }
    public void setAssigned(int assigned) {
        this.assigned = assigned;
    }
}

